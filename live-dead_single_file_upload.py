#!/usr/bin/env python
"""
:authors: Robert C. Moseley (robert.moseley@duke.edu)
"""

import os
import json
import boto3
import logging
import argparse
from botocore.client import Config
from botocore.exceptions import ClientError


def upload_file(file_name, bucket, s3_client, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param s3_client: S3 client
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True


def sd2_upload(yeardate, treatment_date_dir, filename):

    with open('secrets.json') as jfile:
        secrets_json = json.load(jfile)
        s3_config_dict = secrets_json['s3_config']

    s3 = boto3.client(service_name='s3',
                          aws_access_key_id=s3_config_dict['Access Key'],
                          aws_secret_access_key=s3_config_dict['Secret Key'],
                          endpoint_url="https://{}:{}".format(s3_config_dict['Server'],
                                                              s3_config_dict['Port']),
                          config=Config(signature_version='s3v4'),
                          region_name='us-east-1')

    tacc_path = os.path.join(s3_config_dict['Path'], yeardate, treatment_date_dir)

    local_path = os.path.join(os.getcwd(), 'data/upload_data', yeardate, treatment_date_dir)

    file_upload = upload_file(os.path.join(local_path, filename),
                                 s3_config_dict['Bucket'],
                                 s3,
                                 os.path.join(tacc_path, filename))

    print('{} upload: {}'.format(filename, file_upload))


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--filename',
                        help='input file containing CFU data')
    parser.add_argument('-yd', '--yeardate',
                        help='date of experiment in format yyyymm')
    parser.add_argument('-td', '--treatment_date_dir',
                        help='directory of file')

    args = parser.parse_args()
    arg_filename = args.filename
    arg_yeardate = args.yeardate
    arg_treatment_date_dir = args.treatment_date_dir

    sd2_upload(arg_yeardate, arg_treatment_date_dir, arg_filename)
