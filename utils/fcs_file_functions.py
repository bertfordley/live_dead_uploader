#!/usr/bin/env python
"""
:authors: Robert C. Moseley (robert.moseley@duke.edu)
"""

import os
import json


with open('secrets.json') as jfile:
    secrets_json = json.load(jfile)
    FCS_PATH = secrets_json['FCS_PATH']


def fcs_dir(df, exp_date):

    treatments_list = [t for t in set(df['treatment']) if str(t) != 'nan']
    if len(treatments_list) == 1:
        treatments = treatments_list[0]
    else:
        treatments = '-'.join(treatments_list)
    return '_'.join([treatments, exp_date])


def fcs_files_exist(fcs_dir):

    if os.path.isdir(os.path.join(FCS_PATH, fcs_dir)):
        return True
    else:
        return False


def get_fcs_files(fcs_dir):

    treatment_fcs_path = os.path.join(FCS_PATH, fcs_dir)
    fcs_files = [f for f in os.listdir(treatment_fcs_path) if f.endswith('fcs')]
    fcs_files = [f for f in fcs_files if 'beads' not in f.lower()]

    return fcs_files


def add_fcs_filename(row, fcs_file_list, stain=True):

    strain = row['strain']
    rep = 'r{}'.format(row['replicate'])
    treatment = row['treatment']
    if str(treatment) != 'nan':
        conc = '{}{}'.format(int(row['treatment_concentration']), row['treatment_concentration_unit'])
        treatment = '{}_{}'.format(treatment, conc)
    else:
        treatment = 'control'
    if stain:
        string_match = '_'.join([strain, treatment, rep, 'stain']).lower()
    else:
        string_match = '_'.join([strain, treatment, rep, 'no_stain']).lower()

    matched_file = ''
    for file in fcs_file_list:
        if string_match in file.lower():
            matched_file = file

    return matched_file


def add_sytox_info(color):

    color_conc_dict = {'green': 30,
                       'red': 5,
                       'orange': 250}

    return color_conc_dict[color], 'nM'