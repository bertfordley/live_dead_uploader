#!/usr/bin/env python
"""
:authors: Robert C. Moseley (robert.moseley@duke.edu)
"""

import os
import uuid
import time
import pandas as pd

def check_for_beads(fcs_dir, fcs_path):

    treatment_fcs_path = os.path.join(fcs_path, fcs_dir)
    fcs_files = [f for f in os.listdir(treatment_fcs_path) if f.endswith('fcs')]
    fcs_files = [f for f in fcs_files if 'beads' in f.lower()]

    return fcs_files


def add_beads_file(fcs_df, beads_file_list):

    for beads_filename in beads_file_list:

        p_id = uuid.uuid1().hex
        time.sleep(.0001)
        s_id = uuid.uuid1().hex

        e_id = [fcs_df.index.max() + 1]

        beads_df = pd.DataFrame(index=e_id, columns=fcs_df.columns)
        if 'size' in beads_filename.lower():
            beads_df['strain'] = 'beads_spherotech_pps_6K'
            beads_df['control_type'] = 'BEAD_SIZE'
        else:
            beads_df['strain'] = 'beads_spherotech_rainbow'
            beads_df['control_type'] = 'BEAD_FLUORESCENCE'

        beads_df['replicate'] = 1
        beads_df['date_of_experiment'] = fcs_df['date_of_experiment'].tolist()[0]
        beads_df['experiment_reference_url'] = fcs_df['experiment_reference_url'].tolist()[0]
        beads_df['experiment_reference'] = fcs_df['experiment_reference'].tolist()[0]
        beads_df['experiment_id'] = fcs_df['experiment_id'].tolist()[0]
        beads_df['parent_id'] = p_id
        beads_df['strain_class'] = 'Process'

        beads_df['fcs_filename'] = beads_filename
        beads_df['sample_id'] = s_id
        fcs_df = fcs_df.append(beads_df)

    return fcs_df
