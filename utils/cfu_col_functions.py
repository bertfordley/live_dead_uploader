#!/usr/bin/env python
"""
:authors: Robert C. Moseley (robert.moseley@duke.edu)
"""

import pandas as pd


def get_current_data(filename, exp_data):

    df = pd.read_csv(filename)
    return df[df['date_of_experiment'] == int(exp_data)]


def estimate_cells_plated(row):

    cells_plated = row['culture_cells/ml'] / row['PFA-specific dilution'] / row['first dilution'] / \
                   row['second dilution'] * row['volume plated']

    return int(round(cells_plated, 0))


def estimate_cells_per_ml(row):

    cells_per_ml = row['CFU'] * row['PFA-specific dilution'] * row['first dilution'] * \
                   row['second dilution'] / row['volume plated']

    return cells_per_ml


def calucate_percent_killed(row):

    percent_killed = (1 - (row['estimated_cells/ml'] / row['culture_cells/ml'])) * 100

    return round(percent_killed, 2)


def id_controls(row):

    if float(row['treatment_concentration']) == 0.0:
        return 'Control'
    else:
        return 'Experiment'


def define_controls(row):

    if row['strain_class'] == 'Control':
        return 'Negative'


