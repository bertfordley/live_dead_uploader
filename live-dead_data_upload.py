#!/usr/bin/env python
"""
:authors: Robert C. Moseley (robert.moseley@duke.edu)
"""

import os
import json
import uuid
import time
import boto3
import logging
import argparse
import pandas as pd
from datetime import date
from botocore.client import Config
from utils import beads_functions as beads_func, cfu_col_functions as col_func, fcs_file_functions as fcs_func
from botocore.exceptions import ClientError


def generate_unqiue_id(row):
    time.sleep(.0001)
    return uuid.uuid1().hex


def add_yearmonth_to_path(path):
    yearmonth = date.today()
    return os.path.join(path, yearmonth.strftime("%Y%m"))
    # return os.path.join(path, '202006')


def get_er_url(exp_ref):
    with open('secrets.json') as jfile:
        secrets_json = json.load(jfile)
        return secrets_json['ER_urls'][exp_ref]


def check_for_local_dir(treatment_date_dir, upload_bool):
    parent_dir = os.getcwd()
    if upload_bool:
        local_date_path = add_yearmonth_to_path(os.path.join(parent_dir, 'data/upload_data'))
    else:
        local_date_path = add_yearmonth_to_path(os.path.join(parent_dir, 'data/test'))
    local_upload_path = os.path.join(local_date_path, treatment_date_dir)

    if os.path.exists(local_date_path):
        if os.path.exists(local_upload_path):
            pass
        else:
            os.mkdir(local_upload_path)
    else:
        os.mkdir(local_date_path)
        os.mkdir(local_upload_path)

    return local_upload_path


def add_cols_to_cfu(cfu_df, exp_ref, treatment_date_dir, fcs_exist=False):

    er_url = get_er_url(exp_ref)

    # add additional columns
    cfu_df['experiment_reference_url'] = er_url
    cfu_df['experiment_reference'] = exp_ref
    cfu_df['experiment_id'] = treatment_date_dir
    cfu_df['estimated_cells_plated'] = cfu_df.apply(col_func.estimate_cells_plated, axis=1)
    cfu_df['estimated_cells/ml'] = cfu_df.apply(col_func.estimate_cells_per_ml, axis=1)
    cfu_df['percent_killed'] = cfu_df.apply(col_func.calucate_percent_killed, axis=1)
    cfu_df['strain_class'] = cfu_df.apply(col_func.id_controls, axis=1)
    cfu_df['control_type'] = cfu_df.apply(col_func.define_controls, axis=1)
    cfu_df['sample_id'] = cfu_df.apply(generate_unqiue_id, axis=1)

    # add fcs file columns
    if fcs_exist:
        if len(set(cfu_df['fcs_file_sytox'].values)) == 1:
            # for data that does not have the fcs file columns filled
            cfu_df['parent_id'] = cfu_df.apply(generate_unqiue_id, axis=1)
            fcs_files = fcs_func.get_fcs_files(treatment_date_dir)
            cfu_df['fcs_file_sytox'] = cfu_df.apply(fcs_func.add_fcs_filename, fcs_file_list=fcs_files, axis=1)
            cfu_df['fcs_file_no_sytox'] = cfu_df.apply(fcs_func.add_fcs_filename, fcs_file_list=fcs_files, stain=False, axis=1)
        else:
            # for data that does have the fcs file columns filled
            fcs_files = cfu_df['fcs_file_sytox'].tolist() + cfu_df['fcs_file_no_sytox'].tolist()

            cfu_gb = cfu_df.groupby(['strain', 'treatment', 'treatment_concentration', 'treatment_concentration_unit',
                                     'treatment_time', 'treatment_time_unit'])
            df_list = []
            for df_gb in cfu_gb:
                parent_id = uuid.uuid1().hex
                df_gb[1]['parent_id'] = parent_id
                df_list.append(df_gb[1])
            cfu_df = pd.concat(df_list)

        drop_cols = ['PFA-specific dilution', 'first dilution', 'second dilution', 'volume plated']
        cfu_df = cfu_df.drop(drop_cols, axis=1)
        return cfu_df, list(set(fcs_files))
    else:
        drop_cols = ['PFA-specific dilution', 'first dilution', 'second dilution', 'volume plated']
        cfu_df = cfu_df.drop(drop_cols, axis=1)
        return cfu_df


def make_fcs_file(cfu_df, sytox_color):

    if len(set(cfu_df['fcs_file_sytox'].values)) >= 1:
        cfu_gb = cfu_df.groupby(['strain', 'treatment', 'treatment_concentration', 'treatment_concentration_unit',
                                 'treatment_time', 'treatment_time_unit'])
        df_list = []
        for df_gb in cfu_gb:
            df_list.append(df_gb[1].drop(df_gb[1].index[[1,2]]))
        cfu_df = pd.concat(df_list)

    stain_fcs_df = cfu_df.drop('fcs_file_no_sytox', axis=1)
    stain_fcs_df = stain_fcs_df.rename(columns={'fcs_file_sytox': 'fcs_filename'})

    conc, unit = fcs_func.add_sytox_info(sytox_color)
    stain_fcs_df['sytox_color'] = sytox_color
    stain_fcs_df['sytox_concentration'] = conc
    stain_fcs_df['sytox_concentration_unit'] = unit

    no_stain_fcs_df = cfu_df.drop('fcs_file_sytox', axis=1)
    no_stain_fcs_df = no_stain_fcs_df.rename(columns={'fcs_file_no_sytox': 'fcs_filename'})

    fcs_df = pd.concat([stain_fcs_df, no_stain_fcs_df])

    drop_cols = ['sample_id', 'CFU', 'estimated_cells_plated', 'estimated_cells/ml', 'percent_killed']
    fcs_df = fcs_df.drop(drop_cols, axis=1)
    fcs_df['sample_id'] = fcs_df.apply(generate_unqiue_id, axis=1)

    return fcs_df


def upload_file(file_name, bucket, s3_client, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param s3_client: S3 client
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True


def upload_cfu_data(treatment_date_dir, cfu_df_filename, dfs_local_path):

    with open('secrets.json') as jfile:
        secrets_json = json.load(jfile)
        s3_config_dict = secrets_json['s3_config']

    s3 = boto3.client(service_name='s3',
                      aws_access_key_id=s3_config_dict['Access Key'],
                      aws_secret_access_key=s3_config_dict['Secret Key'],
                      endpoint_url="https://{}:{}".format(s3_config_dict['Server'],
                                                          s3_config_dict['Port']),
                      config=Config(signature_version='s3v4'),
                      region_name='us-east-1')

    tacc_path = os.path.join(add_yearmonth_to_path(s3_config_dict['Path']), treatment_date_dir)

    cfu_upload = upload_file(os.path.join(dfs_local_path, cfu_df_filename),
                             s3_config_dict['Bucket'],
                             s3,
                             os.path.join(tacc_path, cfu_df_filename))
    print('CFU upload: {}'.format(cfu_upload))


def upload_fcs_data(treatment_date_dir, fcs_df_filename, dfs_local_path, fcs_files_list, fcs_files_path):

    with open('secrets.json') as jfile:
        secrets_json = json.load(jfile)
        s3_config_dict = secrets_json['s3_config']

    s3 = boto3.client(service_name='s3',
                      aws_access_key_id=s3_config_dict['Access Key'],
                      aws_secret_access_key=s3_config_dict['Secret Key'],
                      endpoint_url="https://{}:{}".format(s3_config_dict['Server'],
                                                          s3_config_dict['Port']),
                      config=Config(signature_version='s3v4'),
                      region_name='us-east-1')

    tacc_path = os.path.join(add_yearmonth_to_path(s3_config_dict['Path']), treatment_date_dir)

    fcs_upload = upload_file(os.path.join(dfs_local_path, fcs_df_filename),
                             s3_config_dict['Bucket'],
                             s3,
                             os.path.join(tacc_path, fcs_df_filename))
    print('FCS upload: {}'.format(fcs_upload))

    for idx, file in enumerate(fcs_files_list):
        os.path.join(fcs_files_path, treatment_date_dir, file)
        fcs_file_upload = upload_file(os.path.join(fcs_files_path, treatment_date_dir, file),
                                      s3_config_dict['Bucket'],
                                      s3,
                                      os.path.join(tacc_path, file))
        print('{} of {} FCS file uploads: {}'.format(idx + 1, len(fcs_files_list), fcs_file_upload))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-er', '--experiment_ref',
                        help='experimental reference')
    parser.add_argument('-f', '--filename',
                        help='input file containing CFU data')
    parser.add_argument('-ed', '--experiment_date',
                        help='date of experiment in format yyyymmdd')
    parser.add_argument('-s', '--styox_color',
                        default='orange',
                        help='the color of sytox used')
    parser.add_argument('-u', '--upload',
                        default=False,
                        help='True/False for upload data to TACC')

    args = parser.parse_args()
    arg_exp_ref = args.experiment_ref
    arg_filename = args.filename
    arg_exp_date = args.experiment_date
    arg_styox_color = args.styox_color
    arg_upload_bool = args.upload

    cfu_df = col_func.get_current_data(arg_filename, arg_exp_date)
    treatment_date_dir = fcs_func.fcs_dir(cfu_df, arg_exp_date)

    fcs_exist = fcs_func.fcs_files_exist(treatment_date_dir)

    # check dir exists, if not, make it
    local_upload_path = check_for_local_dir(treatment_date_dir, arg_upload_bool)

    if fcs_exist:
        cfu_df, fcs_files = add_cols_to_cfu(cfu_df, arg_exp_ref, treatment_date_dir, fcs_exist=fcs_exist)

        fcs_df = make_fcs_file(cfu_df, arg_styox_color)

        cfu_fcs_columns = ['fcs_file_sytox', 'fcs_file_no_sytox']
        cfu_df = cfu_df.drop(cfu_fcs_columns, axis=1)

        # beads
        beads_file = beads_func.check_for_beads(treatment_date_dir, fcs_func.FCS_PATH)
        if beads_file:
            for bfile in beads_file:
                fcs_files.append(bfile)
            fcs_df = beads_func.add_beads_file(fcs_df, beads_file)

        fcs_df_fname = '{}__{}'.format(treatment_date_dir, 'fcs_meta.csv')
        fcs_df.to_csv(os.path.join(local_upload_path, fcs_df_fname), index=False)

    else:
        cfu_df = add_cols_to_cfu(cfu_df, arg_exp_ref, treatment_date_dir)

    # save df to local dir
    cfu_df_fname = '{}__{}'.format(treatment_date_dir, 'cfu_and_meta.csv')
    cfu_df.to_csv(os.path.join(local_upload_path, cfu_df_fname), index=False)

    if arg_upload_bool and fcs_exist:
        upload_cfu_data(treatment_date_dir, cfu_df_fname, local_upload_path)
        upload_fcs_data(treatment_date_dir, fcs_df_fname, local_upload_path, fcs_files, fcs_func.FCS_PATH)
    elif arg_upload_bool and fcs_exist is False:
        upload_cfu_data(treatment_date_dir, cfu_df_fname, local_upload_path)
