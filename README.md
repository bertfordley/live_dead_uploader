# Live Dead Uploader

This project is for uploading experimental data to TACC S3 that consist of CFU and FCS data.

The secrets json must be filled in before use.

# Create Conda Environment
```
conda env create -f conda_req.yml
conda activate live_dead_uploader
```

# Upload all data from a single experiment
```
python live-dead_data_upload.py -er <name of ER> -f data/<master file> -ed <date of experiment -u True 
```
Real example:
```
python live-dead_data_upload.py -er Duke-YeastSTATES-Heat-LiveDeadClassification -f data/Duke_live-dead_CFUs_master.csv -ed 20210618 -u True 
```

# To test aggregation of data, do not set -u to True
```
python live-dead_data_upload.py -er <name of ER> -f data/<master file> -ed <date of experiment 
```
Real example:
```
python live-dead_data_upload.py -er Duke-YeastSTATES-Heat-LiveDeadClassification -f data/Duke_live-dead_CFUs_master.csv -ed 20210618 
```

